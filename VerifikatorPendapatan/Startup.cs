﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VerifikatorPendapatan.Startup))]
namespace VerifikatorPendapatan
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
