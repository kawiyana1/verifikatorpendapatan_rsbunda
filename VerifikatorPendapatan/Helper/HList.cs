﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VerifikatorPendapatan.Entities.SIM;
using Newtonsoft.Json;

namespace VerifikatorPendapatan.Helper
{
    public static class HList
    {
        public static List<SelectListItem> ListBulan
        {
            get
            {
                return new List<SelectListItem>()
                {
                    new SelectListItem(){ Text = "Januari", Value = "1" },
                    new SelectListItem(){ Text = "Febuari", Value = "2" },
                    new SelectListItem(){ Text = "Maret", Value = "3" },
                    new SelectListItem(){ Text = "April", Value = "4" },
                    new SelectListItem(){ Text = "Mei", Value = "5" },
                    new SelectListItem(){ Text = "Juni", Value = "6" },
                    new SelectListItem(){ Text = "Juli", Value = "7" },
                    new SelectListItem(){ Text = "Agustus", Value = "8" },
                    new SelectListItem(){ Text = "September", Value = "9" },
                    new SelectListItem(){ Text = "Oktober", Value = "10" },
                    new SelectListItem(){ Text = "November", Value = "11" },
                    new SelectListItem(){ Text = "Desember", Value = "12" },
                };
            }
        }

        public static string Tipe
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.AU_Tipe.ToList();

                    string r = "";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.Tipe}\">{x.Tipe}</option>";
                    }
                    return r;
                }
            }
        }

        public static string TipePasien
        {
            get
            {
                using (var s = new SIMEntities())
                {
                    var m = s.SIMmJenisKerjasama.ToList();

                    string r = $"<option value=\"ALL\">ALL</option>";
                    foreach (var x in m)
                    {
                        r += $"<option value=\"{x.JenisKerjasama}\">{x.JenisKerjasama}</option>";
                    }
                    return r;
                }
            }
        }
        public static string Posting
        {
            get
            {
                string r = $"<option value=\"ALL\">ALL</option>";
                 r += $"<option value=\"1\">Sudah Posting</option>";
                 r += $"<option value=\"0\">Belum Posting</option>";
                return r;
            }
        }

    }
}