﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace VerifikatorPendapatan.Helper
{
    public static class ConfigModel
    {
        public static bool Develop { get { return true; } }
        public static string UnitName { get { return "Rumah Sakit Wisma Prasanti"; } }
        public static string AppName { get { return "Verifikator Pendapatan"; } }
        public static string Version { get { return "0.1"; } }
    }
}