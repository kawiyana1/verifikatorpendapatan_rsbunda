﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VerifikatorPendapatan.Models
{
    public class PostingKeuanganViewModel
    {
        public List<PostingKeuanganDetailViewModel> detail { get; set; }
    }

    public class PostingKeuanganDetailViewModel
    {
        public string NoBukti { get; set; }
        public bool Posting { get; set; }
    }
}