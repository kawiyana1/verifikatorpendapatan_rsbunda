﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VerifikatorPendapatan.Models
{
    public class PengakuanPendapatanModel
    {
        public string NoBukti { get; set; }
        public System.DateTime Tanggal { get; set; }
        public System.DateTime Jam { get; set; }
        public Nullable<System.DateTime> TglTransaksi { get; set; }
        public string NoReg { get; set; }
        public Nullable<System.DateTime> TglReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKerjasama { get; set; }
        public string Nama_Customer { get; set; }
        public string NoKartu { get; set; }

        public List<DetailPendapatanModel> DetailPendapatan { get; set; }
        public List<DetailTunaiModel> DetailTunai { get; set; }
        public List<DetailPiutangModel> DetailPiutang { get; set; }
    }

    public class DetailPendapatanModel
    {
        public string NoBukti { get; set; }
        public string NoBuktiJurnal { get; set; }
        public string AkunNo { get; set; }
        public string Akun_Name { get; set; }
        public decimal Debet { get; set; }
        public decimal Kredit { get; set; }
        public string Keterangan { get; set; }
    }

    public class DetailTunaiModel
    {
        public string NoBukti { get; set; }
        public string NoBuktiJurnal { get; set; }
        public string AkunNo { get; set; }
        public string Akun_Name { get; set; }
        public decimal Debet { get; set; }
        public decimal Kredit { get; set; }
        public string Keterangan { get; set; }
    }

    public partial class DetailPiutangModel
    {
        public string NoBukti { get; set; }
        public string NoBuktiTransaksi { get; set; }
        public int AkunID { get; set; }
        public string Akun_Name { get; set; }
        public decimal NilaiPiutang { get; set; }
        public short CustomerID { get; set; }
        public string Nama_Customer { get; set; }
        public string Keterangan { get; set; }
    }

    public partial class delete_allviewModel
    {
        public string NoBukti { get; set; }
        public bool Delete { get; set; }
    }
}