﻿using VerifikatorPendapatan.Entities.SIM;
using VerifikatorPendapatan.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using VerifikatorPendapatan.Models;
using System.Data.Entity.Validation;

namespace VerifikatorPendapatan.Controllers
{
    public class PostingKeuanganController : Controller
    {
        #region === POSTING KE KEUANGAN
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string ListPosting(string FromDate, string ToDate, bool AllPeriode, string search, string TipePasien)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.AU_ListPosting.AsQueryable();
                    if (AllPeriode == false)
                    {
                        if (!string.IsNullOrEmpty(FromDate))
                        {
                            var fDate = DateTime.Parse(FromDate);
                            proses = proses.Where(y => y.TglClosing >= fDate);
                        }
                        
                        
                        if (!string.IsNullOrEmpty(ToDate))
                        {
                            var tDate = DateTime.Parse(ToDate);
                            proses = proses.Where(y => y.TglClosing <= tDate);
                        }
                        
                    }

                    if (search != null && search != "")
                    {
                        proses = proses.Where(y =>
                                                y.NoBukti == search ||
                                                y.NoInvoice == search ||
                                                y.NamaPasien == search
                                            );
                    }

                    if (TipePasien != null && TipePasien != "")
                    {
                        if (TipePasien != "ALL")
                        {
                            proses = proses.Where(y =>
                                                y.JenisKerjasama == TipePasien
                                            );
                        }
                    }

                    totalcount = proses.Count();
                    var models = proses.ToList();
                    var r = models.ConvertAll(x => new
                    {
                        NoBukti = x.NoBukti,
                        TglAudit = x.TglAudit.ToString("dd/MM/yyyy"),
                        NoInvoice = x.NoInvoice,
                        TglClosing = x.TglClosing.Value.ToString("dd/MM/yyyy"),
                        NamaPasien = x.NamaPasien,
                        Keterangan = x.Keterangan,
                        JenisKerjasama = x.JenisKerjasama
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        [ActionName("Create")]
        public string Create_post()
        {
            var item = new PostingKeuanganViewModel();
            TryUpdateModel(item);
            try
            {
                using (var s = new SIMEntities())
                {
                    using (var dbContextTransaction = s.Database.BeginTransaction())
                    {
                        var dt = "";
                        var error = 0;
                        var succes = 0;
                        foreach (var x in item.detail)
                        {
                            var d = new
                            {
                                NoBukti = x.NoBukti,
                                Posting = x.Posting,
                            };
                            if (d.Posting == true)
                            {
                                dt = s.Verif_Posting(d.NoBukti).FirstOrDefault();
                            }
                            if (dt == d.NoBukti)
                            {
                                succes += 1;
                            }
                            else
                            {
                                error += 1;
                            }
                        }
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new 
                        { 
                            IsSuccess = true, 
                            Message = "Data berhasil diposting",
                            DataError = error,
                            DataSuccess = succes
                        });
                    }
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }
        #endregion

        #region === CANCEL POSTING KE KEUANGAN
        [HttpGet]
        public ActionResult Cancel()
        {
            return View();
        }
        
        [HttpPost]
        [ActionName("Cancel")]
        public string Cancel_post()
        {
            var item = new PostingKeuanganViewModel();
            TryUpdateModel(item);
            try
            {
                using (var s = new SIMEntities())
                {
                    using (var dbContextTransaction = s.Database.BeginTransaction())
                    {
                        var dt = "";
                        var error = 0;
                        var succes = 0;
                        foreach (var x in item.detail)
                        {
                            var d = new
                            {
                                NoBukti = x.NoBukti,
                                Posting = x.Posting,
                            };
                            if (d.Posting == true)
                            {
                                dt = s.Verif_Posting_Cancel(d.NoBukti).FirstOrDefault();
                            }
                            if (dt == d.NoBukti)
                            {
                                succes += 1;
                            }
                            else
                            {
                                error += 1;
                            }
                        }
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = true,
                            Message = "Data berhasil diposting",
                            DataError = error,
                            DataSuccess = succes
                        });
                    }
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string ListCancelPosting(string FromDate, string ToDate, bool AllPeriode, string search)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.AU_ListCancelPosting.AsQueryable();
                    if (AllPeriode == false)
                    {
                        if (!string.IsNullOrEmpty(FromDate))
                        {
                            var fDate = DateTime.Parse(FromDate);
                            proses = proses.Where(y => y.TglClosing >= fDate);
                        }


                        if (!string.IsNullOrEmpty(ToDate))
                        {
                            var tDate = DateTime.Parse(ToDate);
                            proses = proses.Where(y => y.TglClosing <= tDate);
                        }

                    }

                    if (search != null && search != "")
                    {
                        proses = proses.Where(y =>
                                                y.NoBukti == search ||
                                                y.NoInvoice == search ||
                                                y.NamaPasien == search
                                            );
                    }

                    totalcount = proses.Count();
                    var models = proses.ToList();
                    var r = models.ConvertAll(x => new
                    {
                        NoBukti = x.NoBukti,
                        TglAudit = x.TglAudit.ToString("dd/MM/yyyy"),
                        NoInvoice = x.NoInvoice,
                        TglClosing = x.TglClosing.Value.ToString("dd/MM/yyyy"),
                        NamaPasien = x.NamaPasien,
                        Keterangan = x.Keterangan
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }
        #endregion
    }
}