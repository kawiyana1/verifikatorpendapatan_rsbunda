﻿using VerifikatorPendapatan.Entities.SIM;
using VerifikatorPendapatan.Helper;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using VerifikatorPendapatan.Models;

namespace VerifikatorPendapatan.Controllers
{
    public class PengakuanPendapatanController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.AU_ListAudit.AsQueryable().Where(x => x.Batal == false);
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                            proses = proses.Where(y => y.NRM.Contains(x.Value) ||
                                y.NamaPasien.Contains(x.Value) ||
                                y.NRM.Contains(x.Value) ||
                                y.NoInvoice.ToString().Contains(x.Value) ||
                                y.NoBukti.ToString().Contains(x.Value) ||
                                y.TipePasien.ToString().Contains(x.Value) 
                                );

                        if (x.Key == "Tipe" && !string.IsNullOrEmpty(x.Value) && x.Value != "ALL")
                        {
                            proses = proses.Where(y => y.Tipe.Contains(x.Value));
                        }
                        if (x.Key == "TipePasien" && !string.IsNullOrEmpty(x.Value) && x.Value != "ALL")
                        {
                            proses = proses.Where(y => y.TipePasien.Contains(x.Value));
                        }
                        if (x.Key == "Posting" && !string.IsNullOrEmpty(x.Value) && x.Value != "ALL")
                        {
                            var posting = (x.Value == "1") ? true : false;
                            proses = proses.Where(y => y.Posting == posting);
                        }
                    }
                    if (filter[3].Key == "AllPeriode" && filter[3].Value == "false")
                    {
                        if (filter[1].Key == "FromDate")
                        {
                            if (!string.IsNullOrEmpty(filter[1].Value))
                            {
                                var fDate = DateTime.Parse(filter[1].Value).Date; ;
                                proses = proses.Where(y => y.Tanggal >= fDate);
                            }
                        }
                        if (filter[2].Key == "ToDate")
                        {
                            if (!string.IsNullOrEmpty(filter[2].Value))
                            {
                                var tDate = DateTime.Parse(filter[2].Value).Date;
                                proses = proses.Where(y => y.Tanggal <= tDate);
                            }
                        }
                    }

                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(x => new
                    {
                        NoBukti = x.NoBukti,
                        Tanggal = x.Tanggal.ToString("dd/MM/yyyy"),
                        Jam = x.Jam.ToString("HH:mm"),
                        TipePasien = x.TipePasien,
                        NoInvoice = x.NoInvoice,
                        NRM = x.NRM,
                        NamaPasien = x.NamaPasien,
                        Posting = x.Posting
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, PengakuanPendapatanModel model)
        {

            try
            {
                using (var s = new SIMEntities())
                {
                    
                    var id = "";
                    var userid = User.Identity.GetUserId();
                    if (_process == "DELETE")
                    {
                        id = model.NoBukti;
                        s.AU_DeleteAudit(id);
                        s.SaveChanges();
                    }

                    //var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                    //{
                    //    Activity = $"SetupVendor-{_process}; id:{id};".ToLower()
                    //};
                    //UserActivity.InsertUserActivity(userActivity);

                    return HConvert.Success();
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        [HttpPost]
        public string Detail(string id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.AU_GetHeader.FirstOrDefault(x => x.NoBukti == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var au = s.AU_ListAudit.FirstOrDefault(x => x.NoBukti == id);
                    var dPendapatan = s.AU_GetDetailPendapatan.Where(x => x.NoBukti == m.NoBukti).OrderByDescending(x => x.Debet).ToList();
                    var dTunai= s.AU_GetDetailTunai.Where(x => x.NoBukti == m.NoBukti).OrderByDescending(x => x.Debet).ToList();
                    var dPiutang = s.AU_GetDetailPiutang.Where(x => x.NoBukti == m.NoBukti).OrderBy(x => x.Akun_Name).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            NoBukti = m.NoBukti,
                            Tanggal = m.Tanggal.ToString("dd/MM/yyyy"),
                            Jam = m.Jam.ToString("HH:mm"),
                            TglTransaksi = (m.TglTransaksi == null ? "" : m.TglTransaksi.Value.ToString("dd/MM/yyyy")),
                            NoReg = m.NoReg,
                            TglReg = m.TglReg.Value.ToString("dd/MM/yyyy"),
                            NRM = m.NRM,
                            NamaPasien = m.NamaPasien,
                            JenisKerjasama = m.JenisKerjasama,
                            Nama_Customer = m.Nama_Customer,
                            NoKartu = m.NoKartu,
                            NoInvoice = au.NoInvoice ?? "" ,
                            Batal = au.Batal != true ? 0 : 1,
                            Posting = au.Posting != true ? 0 : 1,
                        },
                        DetailPendapatan = dPendapatan.ConvertAll(x => new
                        {
                            NoBukti = x.NoBukti,
                            NoBuktiJurnal = x.NoBuktiJurnal,
                            AkunNo = x.AkunNo,
                            Akun_Name = x.Akun_Name,
                            Debet = x.Debet,
                            Kredit = x.Kredit,
                            Keterangan = x.Keterangan
                        }),
                        DetailTunai = dTunai.ConvertAll(x => new
                        {
                            NoBukti = x.NoBukti,
                            NoBuktiJurnal = x.NoBuktiJurnal,
                            AkunNo = x.AkunNo,
                            Akun_Name = x.Akun_Name,
                            Debet = x.Debet,
                            Kredit = x.Kredit,
                            Keterangan = x.Keterangan
                        }),
                        DetailPiutang = dPiutang.ConvertAll(x => new
                        {
                            NoBukti = x.NoBukti,
                            NoBuktiTransaksi = x.NoBuktiTransaksi,
                            AkunID = x.AkunID,
                            Akun_Name = x.Akun_Name,
                            NilaiPiutang = x.NilaiPiutang,
                            CustomerID = x.CustomerID,
                            Nama_Customer = x.Nama_Customer,
                            Keterangan = x.Keterangan,
                            
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public string Proses(string FromDate, string Todate)
        {
           
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        s.Database.CommandTimeout = 180;

                        var d = s.Verifikator_Proses(FromDate, Todate).ToList();

                        dbContextTransaction.Commit();

                        if (d == null)
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = false,
                                Data = new
                                {
                                    Datas = 0,
                                    CountAll = 0,
                                    Mssg = "Data tidak ditemukan."
                                },
                            });
                        }
                        else
                        {
                            return JsonConvert.SerializeObject(new
                            {
                                IsSuccess = true,
                                Data = new
                                {
                                    Datas = d,
                                    CountAll = d.Count,
                                    Mssg = $"{d.Count} Data berhasil diproses."
                                },
                            });
                        }
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
             }
           
        }

        [HttpPost]
        public string Posting(string id, string type)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                   
                    if (type == "POSTING")
                    {
                        var dt = s.Verif_Posting(id).FirstOrDefault();
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = (dt == id ? true : false),
                            Data = new
                            {
                                Datas = dt,
                                Mssg = ""
                            },
                        });
                    }
                    else if(type == "CANCEL")
                    {
                        var dt = s.Verif_Posting_Cancel(id).FirstOrDefault();
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = (dt == id ? true : false),
                            Data = new
                            {
                                Datas = dt,
                                Mssg = ""
                            },
                        });
                    }
                    else
                    {
                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = true,
                            Data = new
                            {
                                Datas = "",
                                Mssg = $"Internal serve error."
                            },
                        });
                    }
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #region === CANCEL POSTING KE KEUANGAN

        [HttpPost]
        [ActionName("Cancel")]
        public string delete_all(List<delete_allviewModel> id)
        {

            try
            {
                using (var s = new SIMEntities())
                {
                    using (var dbContextTransaction = s.Database.BeginTransaction())
                    {
                        var dt = "";
                        foreach (var x in id)
                        {
                            var d = new
                            {
                                NoBukti = x.NoBukti,
                                Delete = x.Delete,
                            };
                            if (d.Delete == true)
                            {
                                dt = s.AU_DeleteAudit(x.NoBukti).FirstOrDefault();
                            }
                        }
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new
                        {
                            IsSuccess = true,
                            Message = "Data berhasil diposting",
                        });
                    }
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}